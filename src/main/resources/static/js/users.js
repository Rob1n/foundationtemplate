$(document).ready(function () {

    var datatable2Rest = function (sSource, aoData, fnCallback) {

        //extract name/value pairs into a simpler map for use later
        var paramMap = {};
        for (var i = 0; i < aoData.length; i++) {
            paramMap[aoData[i].name] = aoData[i].value;
        }
        // console.log(paramMap);

        //page calculations
        var pageSize = paramMap.iDisplayLength;
        var start = paramMap.iDisplayStart;
        var pageNum = (start == 0) ? 1 : (start / pageSize) + 1; // pageNum is +1 based // no its not apparently

        // extract sort information
        var sortCol = paramMap.iSortCol_0;
        var sortDir = paramMap.sSortDir_0;
        var sortName = paramMap['mDataProp_' + sortCol];

        //create new json structure for parameters for REST request
        var restParams = new Array();
        // restParams.push({"name": "size", "value": pageSize});
        restParams.push({"name": "limit", "value": pageSize});
        restParams.push({"name": "page", "value": pageNum - 1}); // -1
        restParams.push({"name": "sort", "value": sortName + ',' + sortDir});
        // restParams.push({"name": sortName + ".dir", "value": sortDir});

        //if we are searching by name, override the url and add the name parameter
        var url = sSource;
        if (paramMap.sSearch != '') {
            // ${baseUrl}
            url = "https://localhost:8443/api/users/search/findByUsernameContains?projection=userDatatables";
            restParams.push({"name": "username", "value": paramMap.sSearch});
        }

        //finally, make the request
        $.ajax({
            "dataType": 'json',
            "type": "GET",
            "url": url,
            "data": restParams,
            "success": function (data) {
                // console.log("ajax request data:");
                data.iTotalRecords = data.page.totalElements;
                data.iTotalDisplayRecords = data.page.totalElements;
                // console.log(data);

                if (data.page.totalElements == 0) {
                    data.content = [];
                    data.content.username = "";
                    data.content.role = "";
                    data.content.image = "";
                    data.content.email = "";
                    data.content.id = "";
                }
                // console.log(data);
                fnCallback(data);
            }
        });

    };

    var table = $('#usersTable').DataTable({
        "sAjaxSource": 'https://localhost:8443/api/users?projection=userDatatables',
        "sAjaxDataProp": 'content',
        "aoColumns": [
            {
                mDataProp: 'id'
            }, {
                mDataProp: 'image',
                bSortable: false,
                render: function (data, type, full, meta) {
                    return '<a href="/images/user/' + data + '"><img class="img-responsive" src="/images/user/' + data + '"/></a>';
                }
            }, {
                mDataProp: 'username',
                render: function (data, type, full, meta) {
                    return '<a href="/users/' + data + '">' + data + '</a>';
                }
            }, {
                mDataProp: 'email'
            }, {
                mDataProp: 'role',
                bSortable: false
            }, {
                render: function (data, type, full, meta) {
                    return '';
                },
                bSortable: false,
                defaultContent: ""
            }
        ],
        "bServerSide": true,
        "fnServerData": datatable2Rest,
        language: {
            zeroRecords: "No matching records found",
            emptyTable: "No matching records found"
        }

    });

});

