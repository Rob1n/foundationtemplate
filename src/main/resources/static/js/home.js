$(document).ready(function () {

    var warningMessage = $('#workingMessage');
    var successMessage = $('#returnMessage');

    var progressBar = $('#progressBarId');
    var progressBarText = $('#progressBarText');

    var ajaxInterval;

    $('#generateForm').submit(function (e) {
        e.preventDefault();

        warningMessage.removeClass("hidden");
        warningMessage.html("<p>Working, please wait...</p>");

        successMessage.addClass("hidden");

        progressBar.attr("aria-valuenow", 0);
        progressBar.css("width", "0%");

        submitAjaxGenerate();
        ajaxInterval = setInterval(queryProgress, 1000);
    });

    function submitAjaxGenerate() {

        var toGenerate = $('#toGenerate').val();

        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/generate',
            data: {
                'toGenerate': toGenerate
            },
            success: function (data) {
                warningMessage.addClass("hidden");

                successMessage.removeClass("hidden");
                successMessage.html("<p>Successfully added " + data.usersCreated + " new users</p>" +
                    "<p>Duration: " + data.duration + " ms</p>");

                queryProgress();
                clearInterval(ajaxInterval);

            },
            error: function () {
                console.log("error");
                clearInterval(ajaxInterval);
            }
        });

    }


    function queryProgress() {
        console.log();
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/getGenerationProgress',
            success: function (data) {
                progressBar.attr("aria-valuenow", data);
                progressBar.css("width", data+"%");
            },
            error: function (data) {
                console.log("error");
            }
        });
    }


// function checkProgress() {
//     $('#progressBarId').attr("aria-valuenow", 10);
//     console.log("done");
// }
//
// (function worker() {
//     $.ajax({
//         url: 'http://8080/getGenerationProgress',
//         success: function (data) {
//             console.log("progress: " + data);
//             // $('.result').html(data);
//         },
//         complete: function () {
//             // Schedule the next request when the current one's complete
//             setTimeout(worker, 500);
//         }
//     });
// })();

});


