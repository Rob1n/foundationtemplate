INSERT INTO user_images (id, url) VALUES
  ('1', '_default.png'),
  ('2', 'admin.jpg'),
  ('3', 'foo.jpg');

INSERT INTO roles (id, name) VALUES
  ('1', 'ROLE_ADMIN'),
  ('2', 'ROLE_USER');

INSERT INTO users (id, username, email, password, enabled, image_id) VALUES
  ('1', 'admin', 'admin@imenik.hr', '$2a$10$ti3BT0/1JqOVI6UBjSA7hOwk.V8lyHMzwYhJOp8yGL/hzsGEfXgHe', TRUE, '2'),
  ('2', 'foo', 'foo@bar.com', '$2a$10$56aC7ASmqTAnRVNog7ug/uoryexzzuA1tvovOyzBfmmilwgOgbQhq', TRUE, '3');

INSERT INTO users_roles (id, users_id, roles_id) VALUES
  ('1', '1', '1'),
  ('2', '1', '2'),
  ('3', '2', '2');


