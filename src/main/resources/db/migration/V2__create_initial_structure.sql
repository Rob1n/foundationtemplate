CREATE TABLE `user_images` (
  `id`  BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = latin1;


CREATE TABLE `roles` (
  `id`   BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255)        DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = latin1;


CREATE TABLE `users` (
  `id`       BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `enabled`  BIT(1)       NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `username` VARCHAR(30)  NOT NULL,
  `email`    VARCHAR(30)  NOT NULL,
  `image_id` BIGINT(20)            DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2erb825q9h3yj977sd61beno5` (`image_id`),
  CONSTRAINT `FK2erb825q9h3yj977sd61beno5` FOREIGN KEY (`image_id`) REFERENCES `user_images` (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = latin1;


CREATE TABLE `users_roles` (
  `id`       BIGINT(20) NOT NULL AUTO_INCREMENT,
  `roles_id` BIGINT(20)          DEFAULT NULL,
  `users_id` BIGINT(20)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa62j07k5mhgifpp955h37ponj` (`roles_id`),
  KEY `FKml90kef4w2jy7oxyqv742tsfc` (`users_id`),
  CONSTRAINT `FKa62j07k5mhgifpp955h37ponj` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FKml90kef4w2jy7oxyqv742tsfc` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = latin1;

