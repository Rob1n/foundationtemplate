DROP PROCEDURE IF EXISTS createUserWithImageProcedure;
DELIMITER //
CREATE PROCEDURE createUserWithImageProcedure(IN in_username VARCHAR(30),
                                              IN in_email    VARCHAR(255),
                                              IN in_password VARCHAR(255),
                                              IN in_enabled  BIT(1),
                                              IN in_image_id BIGINT(20))
  BEGIN
    INSERT INTO users (username, email, password, enabled, image_id) VALUES
      (in_username, in_email, in_password, in_enabled, in_image_id);
  END//
DELIMITER ;


DROP PROCEDURE IF EXISTS createUserWithoutImageProcedure;
DELIMITER //
CREATE PROCEDURE createUserWithoutImageProcedure(IN in_username VARCHAR(30),
                                                 IN in_email    VARCHAR(255),
                                                 IN in_password VARCHAR(255),
                                                 IN in_enabled  BIT(1))
  BEGIN
    INSERT INTO users (username, email, password, enabled) VALUES
      (in_username, in_email, in_password, in_enabled);
  END//
DELIMITER ;


DROP PROCEDURE IF EXISTS createUserRolesProcedure;
DELIMITER //
CREATE PROCEDURE createUserRolesProcedure(IN in_id_roles BIGINT(20),
                                          IN in_id_users BIGINT(20))
  BEGIN
    INSERT INTO users_roles (id_users, id_roles) VALUES
      (in_id_users, in_id_roles);
  END//
DELIMITER ;