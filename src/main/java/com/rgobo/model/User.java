package com.rgobo.model;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "users", indexes = {@Index(name = "IDX_USERS_ID", columnList = "id, username, email")})
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "createUserWithImageProcedure",
                procedureName = "createUserWithImageProcedure",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "username", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "email", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "password", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "enabled", type = Boolean.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "image_id", type = Long.class)
                }
        ),
        @NamedStoredProcedureQuery(name = "createUserWithoutImageProcedure",
                procedureName = "createUserWithoutImageProcedure",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "username", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "email", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "password", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "enabled", type = Boolean.class)
                }
        )
})
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @NotEmpty
    @Length(min = 2, max = 30)
    private String username;

    @NotNull
    @NotEmpty
    @Email
    private String email;

    @NotNull
    @NotEmpty
    @JsonIgnore
    private String password;

    @Transient
    @JsonIgnore
    private String confirmPassword;

    @JsonIgnore
    private boolean enabled;

    @OneToOne(targetEntity = UserImage.class)
    @JsonManagedReference
    private UserImage image;

    @ManyToMany(targetEntity = Role.class, fetch = FetchType.EAGER)
    @JsonManagedReference //to prevent recursion in json
    private List<Role> roles = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<VaultEntry> vaultEntries;

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public User(String username, String email, String password, String confirmPassword) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getRolesString(){
        String rol = "";
//        for (Role r : roles){
//            rol = String.join(", ", rol, r.getName().name());
//        }
        rol = String.join(",", getRoles().toString());
        return rol;
//        return rol;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public UserImage getImage() {
        return image;
    }

    public void setImage(UserImage image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<VaultEntry> getVaultEntries() {
        return vaultEntries;
    }

    public void setVaultEntries(List<VaultEntry> vaultEntries) {
        this.vaultEntries = vaultEntries;
    }
}


