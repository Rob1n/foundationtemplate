package com.rgobo.model.projection;

import com.rgobo.model.Role;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "roleP", types = {Role.class})
public interface RoleP {

    @Value("#{target.id}")
    Long getId();

    @Value("#{target.name.toString()}")
    String getName();

}
