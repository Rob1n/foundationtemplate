package com.rgobo.model.projection;

import com.rgobo.model.Role;
import com.rgobo.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "user", types = {User.class})
public interface UserP {

    @Value("#{target.id}")
    Long getId();

    @Value("#{target.roles}")
    List<Role> getRoles();

    @Value("#{target.username}")
    String getUsername();

    @Value("#{target.email}")
    String getEmail();

    @Value("#{target.image.url}")
    String getImage();


}
