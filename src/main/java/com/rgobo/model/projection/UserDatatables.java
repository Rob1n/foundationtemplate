package com.rgobo.model.projection;

import com.rgobo.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "userDatatables", types = {User.class})
public interface UserDatatables {

    @Value("#{target.id}")
    String getId();

    @Value("#{target.image.url}")
    String getImage();

    @Value("#{target.username}")
    String getUsername();

    @Value("#{target.email}")
    String getEmail();

    @Value("#{target.getRolesString()}")
    String getRole();


}