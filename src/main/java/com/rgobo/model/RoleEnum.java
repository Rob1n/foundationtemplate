package com.rgobo.model;

public enum RoleEnum {
    ROLE_USER,
    ROLE_ADMIN
}
