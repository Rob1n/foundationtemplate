package com.rgobo.model;


import org.springframework.security.core.authority.AuthorityUtils;

import java.util.List;

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public CurrentUser(User user) {
        super(user.getUsername(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getRoles().toString()));
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return user.getId();
    }

    public List<Role> getRoles() {
        return user.getRoles();
    }

    @Override
    public String toString() {
        return "CurrentUser{user=" + user + "} " + super.toString();
    }

}
