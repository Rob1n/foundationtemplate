package com.rgobo.config;

import com.rgobo.model.Role;
import com.rgobo.model.User;
import com.rgobo.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
class CustomAuthenticationProvider implements AuthenticationProvider {

	private final UserService userService;
	private final BCryptPasswordEncoder passwordEncoder;

	@Autowired
	public CustomAuthenticationProvider(UserService userService, BCryptPasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		if (authentication.getName() == null){
			throw new AuthenticationCredentialsNotFoundException("Username empty");
		}

		Long userId = userService.getIdByName(authentication.getName());
		if (userId == null || userId.equals(0L)) {
			throw new AuthenticationCredentialsNotFoundException("User Id not found");
		}

		User user = userService.get(userId);
		if (user == null) {
			throw new AuthenticationCredentialsNotFoundException("User not found");
		}

		if (!user.isEnabled()){
			throw new AuthenticationCredentialsNotFoundException("Account is not yet activated");
		}

		String enteredPassword = authentication.getCredentials().toString();
		String storedPassword = user.getPassword();

		if (!passwordEncoder.matches(enteredPassword, storedPassword)){
			throw new AuthenticationCredentialsNotFoundException("Incorrect password");
		}

		List<GrantedAuthority> grantedAuths = new ArrayList<>();
		for (Role role : user.getRoles()){
			grantedAuths.add(new SimpleGrantedAuthority(role.getName().toString()));
		}

		return new UsernamePasswordAuthenticationToken(
				authentication.getName(), authentication.getCredentials(), grantedAuths);

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
