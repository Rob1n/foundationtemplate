package com.rgobo.config;

import com.rgobo.service.user.CurrentUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private final CustomAuthenticationProvider customAuthenticationProvider;
	private final CurrentUserDetailsService userDetailsService;

	@Autowired
	public WebSecurityConfiguration(CustomAuthenticationProvider customAuthenticationProvider, CurrentUserDetailsService userDetailsService) {
		this.customAuthenticationProvider = customAuthenticationProvider;
		this.userDetailsService = userDetailsService;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/", "/home.html", "/register", "/login/**", "/css/**", "/bootstrap/**", "/api/**")
					.permitAll()
//				.anyRequest().authenticated()
				.and()
				.formLogin()
					.loginPage("/login")
					.failureUrl("/login?error")
//					.defaultSuccessUrl("/home", true)
					.permitAll()
				.and()
				.logout()
					.logoutUrl("/logout")
					.deleteCookies("remember-me")
					.logoutSuccessUrl("/login?logout")
					.permitAll()
				.and();
//				.rememberMe();

		// need this so h2 console works. also logout needs to be POST if this is enabled
		http.csrf().disable(); //TODO re-enable once done testing REST api
//		http.headers().frameOptions().disable();

	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
				.authenticationProvider(customAuthenticationProvider)
				.userDetailsService(userDetailsService);
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// "ROLE_" is automatically added
		auth.inMemoryAuthentication().withUser("user").password("pass").roles("USER");
	}

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(customAuthenticationProvider);
	}
}
