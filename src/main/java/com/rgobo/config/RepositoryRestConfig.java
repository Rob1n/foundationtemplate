package com.rgobo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.http.MediaType;

@Configuration
public class RepositoryRestConfig extends RepositoryRestMvcConfiguration {

    @Override
    public RepositoryRestConfiguration config() {
        RepositoryRestConfiguration config = super.config();
        config.setDefaultMediaType(MediaType.APPLICATION_JSON); // disable HAL as the default hypermedia representation format
        config.setBasePath("/api");

//		config.setReturnBodyOnCreate(Boolean.TRUE);
//		config.setReturnBodyOnUpdate(Boolean.TRUE);
//		config.exposeIdsFor(Project.class);

//		config.setRepositoryDetectionStrategy(RepositoryDetectionStrategy.RepositoryDetectionStrategies.ANNOTATED);
//		config.getProjectionConfiguration().addProjection(UserP.class);
        config.setPageParamName("page")
                .setLimitParamName("limit")
                .setSortParamName("sort");
        return config;
    }

}
