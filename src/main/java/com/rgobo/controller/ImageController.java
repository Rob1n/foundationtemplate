package com.rgobo.controller;

import com.rgobo.model.User;
import com.rgobo.service.image.ImageService;
import com.rgobo.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ImageController {

    private final ImageService imageService;
    private final UserService userService;

    @Autowired
    public ImageController(ImageService imageService, UserService userService) {
        this.imageService = imageService;
        this.userService = userService;
    }

    @RequestMapping("/images/upload")
    public String uploadImage(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes, Authentication auth){

        if (file.isEmpty()){
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:/users/"+auth.getPrincipal().toString()+".html";
        }

        User tUser = userService.getUserByName(auth.getPrincipal().toString());
        imageService.uploadUserImageAndUpdateUser(file, tUser);
        return "redirect:/users/" + auth.getPrincipal().toString() + ".html";

    }

}
