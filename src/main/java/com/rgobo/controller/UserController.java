package com.rgobo.controller;

import com.rgobo.model.User;
import com.rgobo.service.image.ImageService;
import com.rgobo.service.user.UserService;
import com.rgobo.service.user.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserService userService;
    private final ImageService imageService;
    private final UserValidator userValidator;

    @Autowired
    public UserController(UserService userService, ImageService imageService, UserValidator userValidator) {
        this.userService = userService;
        this.imageService = imageService;
        this.userValidator = userValidator;
    }

    /* Login & Registration */
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/register")
    public String getUsersNew(@ModelAttribute("user") User user) {
        return "users-new";
    }

    @PostMapping("/register")
    public String createUser(@Valid @ModelAttribute("user") User user, BindingResult result,
                             @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        result = userValidator.validateCreateUser(user, result);
        if (result.hasErrors()) {
            logger.info("UserController - result set has errors");
            return "users-new";
        }

        userService.create(user, imageService.uploadUserImage(file, user.getUsername()));
        redirectAttributes.addFlashAttribute("message", "Please log in with your new credentials.");
        return "redirect:login.html";
    }

    /* Users */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping("/users")
    public String usersIndex() {
        return "users";
    }

    @PreAuthorize("authentication.name == #username OR hasRole('ROLE_ADMIN')")
    @RequestMapping("/users/{username}")
    public String viewUser(@PathVariable("username") String username, Model model) {
        model.addAttribute("user", userService.getUserByName(username));
        return "users-view";
    }

    @PreAuthorize("authentication.name == #username OR hasRole('ROLE_ADMIN')")
    @GetMapping("/users/{username}/update")
    public String getUpdateUser(@PathVariable("username") String username, @ModelAttribute("user") User user, Model model) {
        model.addAttribute(userService.getUserByName(username));
        return "users-update";
    }

    @PreAuthorize("authentication.name == #username")
    @PostMapping("/users/{username}/update")
    public String updateUser(@PathVariable("username") String username,
                             @Valid @ModelAttribute("user") User user, BindingResult result,
                             @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        User selectedUser = userService.getUserByName(username);
        result = userValidator.validateUpdateUser(user, result, selectedUser);
        if (result.hasErrors()) {
            return "users-update";
        }
        User tmp = userService.update(selectedUser.getId(), user, imageService.uploadUserImage(file, user.getUsername()));
        redirectAttributes.addFlashAttribute("message", "Account successfully updated");
        return "redirect:/users/" + username + ".html";
    }

    @PreAuthorize("authentication.name == #username OR hasRole('ROLE_ADMIN')")
    @RequestMapping("/users/{username}/delete")
    public String deleteUser(@PathVariable("username") String username, HttpServletRequest request, Authentication auth) {
        if (username.equals(auth.getPrincipal().toString())) {
            logOutUser(request, auth);
            userService.deleteByUsername(username);
            return "redirect:/login?logout.html";
        } else {
            userService.deleteByUsername(username);
            return usersIndex();
        }
    }

    /**
     * Nuke the login of authenticated (just deleted) user
     *
     * @param request HttpServletRequest
     * @param auth    user deleting his profile
     */
    private void logOutUser(HttpServletRequest request, Authentication auth) {
        try {
            request.logout();
            SecurityContextHolder.clearContext();
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
            }
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
}
