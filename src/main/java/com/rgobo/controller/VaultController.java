package com.rgobo.controller;

import com.rgobo.model.User;
import com.rgobo.model.VaultEntry;
import com.rgobo.service.user.UserService;
import com.rgobo.service.vault.VaultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class VaultController {
    private final Logger logger = LoggerFactory.getLogger(VaultController.class);


    private final VaultService vaultService;
    private final UserService userService;

    @Autowired
    public VaultController(VaultService vaultService, UserService userService) {
        this.vaultService = vaultService;
        this.userService = userService;
    }

    @RequestMapping("/vault")
    public String getVault(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedIn = userService.getUserByName(auth.getName());
        model.addAttribute("vaultEntries", vaultService.getVaultFor(loggedIn));
        return "vault";
    }

    @GetMapping("/vault/new")
    public String addVaultEntryGet(@ModelAttribute("vaultEntry")VaultEntry vaultEntry) {
        return "vault-new";
    }

    @PostMapping("/vault/new")
    public String addVaultEntryPost(@ModelAttribute("vaultEntry")VaultEntry vaultEntry){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedIn = userService.getUserByName(auth.getName());
        vaultService.addNewVaultEntry(vaultEntry, loggedIn);
        return "redirect:/vault.html";
    }

    @GetMapping("/vault/{id}/delete")
    public String deleteVaultEntry(@PathVariable("id") Long id){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User loggedIn = userService.getUserByName(auth.getName());
        if (!vaultService.getOne(id).getUser().getUsername().equals(loggedIn.getUsername())) {
            return "redirect:/vault.html";
        }
        vaultService.deleteVaultEntryById(id);
        return "redirect:/vault.html";
    }

}
