package com.rgobo.controller;

import com.rgobo.service.home.HomeServiceImpl;
import com.rgobo.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.LinkedHashMap;

@Controller
public class HomeController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String ERROR_PATH = "/error";

    private final HomeServiceImpl homeService;
    private final UserService userService;

    @Autowired
    public HomeController(HomeServiceImpl homeService, UserService userService) {
        this.homeService = homeService;
        this.userService = userService;
    }


    @RequestMapping("/")
    public String home() {
        return "home";
    }

    @RequestMapping("/home")
    public String home2() {
        return "home";
    }

    @GetMapping("/mail")
    public String sendMail() {
        homeService.sendMail();
        return "home";
    }

    @RequestMapping("/generate")
    public @ResponseBody LinkedHashMap<String, Object> generateUsers(@RequestParam("toGenerate") int toGenerate, RedirectAttributes redirectAttributes) {
        return homeService.generateXUsers(toGenerate, redirectAttributes);
//        return "redirect:home.html";
    }

    @RequestMapping("/getUsers")
    public @ResponseBody LinkedHashMap<String, Object> getUsers() {
        return homeService.generateCustomUserJSON();
    }

    @RequestMapping("/getGenerationProgress")
    public @ResponseBody int getGenerationProgress() {
        return homeService.getGenerationProgress();
    }

//    @RequestMapping(ERROR_PATH)
//    public String error(Model model, Exception e){
//        model.addAttribute("errorMessage", e.getMessage());
//        return "403";
//    }
//
//    @Override
//    public String getErrorPath() {
//        return ERROR_PATH;
//    }

}


