package com.rgobo.service.image;

import com.rgobo.model.User;
import com.rgobo.model.UserImage;
import com.rgobo.repository.UserImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageServiceImpl implements ImageService {

    private static UserImage STORED_DEFAULT_IMAGE;

    private final Path rootLocation;

    private final UserImageRepository userImageRepository;

    @Autowired
    public ImageServiceImpl(UserImageRepository userImageRepository) {
        this.userImageRepository = userImageRepository;
        this.rootLocation = Paths.get("src/main/webapp/images/user");
    }

    @Override
    public UserImage uploadUserImage(MultipartFile file, String name) {
        try {
            if (file.isEmpty()) {
                return null;
            }

            String fileName = name + ".jpg";
            Files.deleteIfExists(rootLocation.resolve(fileName));
            Files.copy(file.getInputStream(), rootLocation.resolve(fileName));

            UserImage userImage = new UserImage(fileName);
            saveUserImage(userImage);
            return userImage;

        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void uploadUserImageAndUpdateUser(MultipartFile file, User tUser) {
        try {
            if (file.isEmpty()) {
//                throw new Exception("Failed to store empty file " + file.getOriginalFilename());
                System.out.println("FILE IS EMPTY");
                return;
            }

            String fileName = tUser.getUsername() + ".jpg";
            Files.deleteIfExists(rootLocation.resolve(fileName));
            Files.copy(file.getInputStream(), rootLocation.resolve(fileName));

            if (tUser.getImage() == null) {
                UserImage uImg = new UserImage(fileName);
                saveUserImage(uImg);
                tUser.setImage(uImg);
            }

        } catch (Exception e) {
            System.out.println("FAILED TO STORE FILE");
//            throw new Exception("Failed to store file " + file.getOriginalFilename(), e);
        }
    }

    @Override
    @Transactional
    public UserImage getUserImage(Long id) {
        return userImageRepository.getOne(id);
    }

    @Override
    @Transactional
    public UserImage saveUserImage(UserImage image) {
        return userImageRepository.saveAndFlush(image);
    }

    @Override
    @Transactional
    public UserImage getDefaultImage(){
        if (STORED_DEFAULT_IMAGE == null){
            STORED_DEFAULT_IMAGE = userImageRepository.getOne(1L);
        }
        return STORED_DEFAULT_IMAGE;
    }

}
