package com.rgobo.service.image;

import com.rgobo.model.User;
import com.rgobo.model.UserImage;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {

    UserImage uploadUserImage(MultipartFile file, String username);

    void uploadUserImageAndUpdateUser(MultipartFile file, User user);

    UserImage getUserImage(Long id);

    UserImage saveUserImage(UserImage image);

    UserImage getDefaultImage();
}
