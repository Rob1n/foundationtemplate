package com.rgobo.service.vault;

import com.rgobo.model.User;
import com.rgobo.model.VaultEntry;
import org.springframework.security.core.Authentication;

import java.net.Authenticator;
import java.util.List;

public interface VaultService {
    VaultEntry getOne(Long id);

    public List<VaultEntry> getVault();

    List<VaultEntry> getVaultFor(User user);

    public VaultEntry addNewVaultEntry(VaultEntry vaultEntry, User user);

    VaultEntry deleteVaultEntryById(Long id);
}
