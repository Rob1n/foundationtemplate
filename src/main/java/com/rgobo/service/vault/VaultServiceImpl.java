package com.rgobo.service.vault;

import com.rgobo.model.User;
import com.rgobo.model.VaultEntry;
import com.rgobo.repository.VaultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VaultServiceImpl implements VaultService {

    private final VaultRepository vaultRepository;

    @Autowired
    public VaultServiceImpl(VaultRepository vaultRepository) {
        this.vaultRepository = vaultRepository;
    }

    @Override
    public VaultEntry getOne(Long id){
        return vaultRepository.getOne(id);
    }

    @Override
    public List<VaultEntry> getVault() {
        return vaultRepository.findAll();
    }

    @Override
    public List<VaultEntry> getVaultFor(User user) {
        return vaultRepository.findByUser_Id(user.getId());
    }

    @Override
    public VaultEntry addNewVaultEntry(VaultEntry vaultEntry, User user) {
        vaultEntry.setUser(user);
        return vaultRepository.saveAndFlush(vaultEntry);
    }

    @Override
    public VaultEntry deleteVaultEntryById(Long id) {
        VaultEntry vaultEntry = getOne(id);
        vaultRepository.delete(id);
        return vaultEntry;
    }

}
