package com.rgobo.service.role;

import com.rgobo.model.Role;
import com.rgobo.model.RoleEnum;
import com.rgobo.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleServiceImpl implements RoleService {

    private static Role STORED_ROLE_ADMIN;
    private static Role STORED_ROLE_USER;

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    @Transactional
    public Role get(Long id) {
        return roleRepository.findOne(id);
    }

    @Override
    public Long getIdByName(RoleEnum name) {
        Role tRole = getRoleByName(name);
        if (tRole == null) {
            return null;
        }
        return tRole.getId();
    }

    @Override
    @Transactional
    public Role getRoleByName(RoleEnum name) {
        switch (name){
            case ROLE_USER:
                if (STORED_ROLE_USER == null){
                    STORED_ROLE_USER = roleRepository.findByName(RoleEnum.ROLE_USER);
                }
                return STORED_ROLE_USER;
            case ROLE_ADMIN:
                if (STORED_ROLE_ADMIN == null){
                    STORED_ROLE_ADMIN = roleRepository.findByName(RoleEnum.ROLE_ADMIN);
                }
                return STORED_ROLE_ADMIN;
        }
        return roleRepository.findByName(name);
    }
}
