package com.rgobo.service.role;

import com.rgobo.model.Role;
import com.rgobo.model.RoleEnum;

public interface RoleService {

    Role get(Long id);

    Long getIdByName(RoleEnum name);

    Role getRoleByName(RoleEnum name);
}