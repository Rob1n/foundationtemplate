package com.rgobo.service.user;

import com.rgobo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@Service
public class UserValidatorImpl implements UserValidator {

    private final UserService userService;
    private final int lowerPasswordBound, upperPasswordBound;
    private final String passwordRange;

    @Autowired
    public UserValidatorImpl(UserService userService) {
        this.userService = userService;
        this.lowerPasswordBound = 3;
        this.upperPasswordBound = 20;
        this.passwordRange = " " + lowerPasswordBound + " - " + upperPasswordBound;
    }

    @Override
    public BindingResult validateCreateUser(User user, BindingResult result) {
        if (userService.getUserByName(user.getUsername()) != null) {
            result.rejectValue("username", "user.controller.error.usernameinuse", "Admin alert - missing property! but also username already in use");
        }
        if (!user.getPassword().equals(user.getConfirmPassword())) {
            result.rejectValue("confirmPassword", "user.controller.error.passwords", "Admin alert - missing property! but also passwords do not match");
        }
        if (user.getPassword().isEmpty() || user.getPassword() == null) {
            result.rejectValue("password", "user.controller.error.emptyPassword", "Admin alert - missing property! but also password field empty");
        }
        if (user.getPassword() != null && !inRange(lowerPasswordBound, upperPasswordBound, user.getPassword())) {
            result.rejectValue("password", "user.controller.error.passwordRange", "Admin alert - missing property! but also password not in range");
        }

        return result;
    }

    @Override
    public BindingResult validateUpdateUser(User user, BindingResult result, User selectedUser) {
//        if (!user.getUsername().equals(selectedUser.getUsername()) && (userService.getUserByName(user.getUsername()) != null)) {
//            result.rejectValue("username", "user.controller.error.usernameinuse", "Admin alert - missing property! but also username already in use");
//        }
        if (!user.getPassword().equals(user.getConfirmPassword())) {
            result.rejectValue("confirmPassword", "user.controller.error.passwords", "Admin alert - missing property! but also passwords do not match");
        }
        if (user.getPassword().isEmpty() || user.getPassword() == null) {
            result.rejectValue("password", "user.controller.error.emptyPassword", "Admin alert - missing property! but also password field empty");
        } else if (user.getPassword() != null && !inRange(lowerPasswordBound, upperPasswordBound, user.getPassword())) {
            result.rejectValue("password", "user.controller.error.passwordRange", "Admin alert - missing property! but also password not in range");
        }

        return result;
    }


    public boolean inRange(int lowerBound, int upperBound, String input) {
        return (input.length() >= lowerBound && input.length() <= upperBound);
    }

}
