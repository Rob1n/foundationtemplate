package com.rgobo.service.user;

import com.rgobo.model.User;
import com.rgobo.model.UserImage;

import java.util.List;

public interface UserService {

    User create(User user, UserImage image);

    void batchSave(List<User> usersToSave);

    User get(Long id);

    User update(Long id, User user, UserImage image);

    User delete(Long id);

    List<User> getAllUsers();

    Long getIdByName(String name);

    User deleteByUsername(String username);

//    void createVerificationToken(User user, String token);
//
//    VerificationToken getVerificationToken(String token);
//
//    void verifyUser(VerificationToken token);

    boolean canAccessUserPage(User user, String username);

    User getUserByName(String name);

}
