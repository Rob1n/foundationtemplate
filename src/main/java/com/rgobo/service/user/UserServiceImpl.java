package com.rgobo.service.user;

import com.rgobo.model.Role;
import com.rgobo.model.RoleEnum;
import com.rgobo.model.User;
import com.rgobo.model.UserImage;
import com.rgobo.repository.UserRepository;
import com.rgobo.service.image.ImageService;
import com.rgobo.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @PersistenceContext
    private EntityManager entityManager;

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;
    private final ImageService imageService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleService roleService, ImageService imageService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
        this.imageService = imageService;
    }

    @Override
    @Transactional
    public User create(User user, UserImage image) {
        String bPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(bPassword);

        Role role = roleService.getRoleByName(RoleEnum.ROLE_USER);
        user.getRoles().add(role);

        user.setEnabled(true);

        if (image == null){
            image = imageService.getDefaultImage();
        }
        user.setImage(image);

        return userRepository.saveAndFlush(user);

//        if (user.getImage() != null) {
//            userRepository.createUserWithImageProcedure(
//                    user.getUsername(), user.getEmail(), user.getPassword(), user.isEnabled(), user.getImage().getId());
//        } else {
//            userRepository.createUserWithoutImageProcedure(
//                    user.getUsername(), user.getEmail(), user.getPassword(), user.isEnabled());
//        }
//        return user;
    }

    @Override
    @Transactional
    public void batchSave(List<User> usersToSave) {
        int batchSize = 25;
        try {
            int i = 0;
            for (User u : usersToSave) {
                u.getRoles().add(roleService.getRoleByName(RoleEnum.ROLE_USER));
                if (u.getImage() == null){
                    u.setImage(imageService.getDefaultImage());
                }
                entityManager.persist(u);

                if (i > 0 && i % batchSize == 0) {
                    //flush a batch of inserts and release memory
                    entityManager.flush();
                    entityManager.clear();
                }
                i++;
            }
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    @Override
    @Transactional
    public User get(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    @Transactional
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public Long getIdByName(String name) {
        User tUser = getUserByName(name);
        if (tUser == null) {
            return null;
        }
        return tUser.getId();
    }

    @Override
    @Transactional
    public User getUserByName(String name) {
        List<User> tUsers = userRepository.findByUsername(name);
        if (tUsers == null || tUsers.isEmpty()) {
            return null;
        }
        return tUsers.get(0);
    }

    @Override
    @Transactional
    public User update(Long id, User user, UserImage image) {
        User oldUser = userRepository.getOne(id);
        oldUser.setUsername(user.getUsername());
        oldUser.setEmail(user.getEmail());
        oldUser.setPassword(passwordEncoder.encode(user.getPassword()));
        oldUser.setImage(image);
        return userRepository.saveAndFlush(oldUser);
    }

    @Override
    @Transactional
    public User delete(Long id) {
        User tUser = get(id);
        if (tUser == null) {
            return null;
        }
        userRepository.delete(tUser);
        return tUser;
    }

    @Override
    @Transactional
    public User deleteByUsername(String username) {
        User tUser = getUserByName(username);
        if (tUser == null) {
            return null;
        }
        userRepository.delete(tUser);
        return tUser;
    }


    /* Authentication */
    @Override
    public boolean canAccessUserPage(User user, String username) {
        return false;
    }


}
