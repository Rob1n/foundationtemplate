package com.rgobo.service.user;

import com.rgobo.model.User;
import org.springframework.validation.BindingResult;

public interface UserValidator {
    BindingResult validateCreateUser(User user, BindingResult result);

    BindingResult validateUpdateUser(User user, BindingResult result, User currentUser);
}
