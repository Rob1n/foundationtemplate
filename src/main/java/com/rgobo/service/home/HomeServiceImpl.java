package com.rgobo.service.home;

import com.rgobo.model.User;
import com.rgobo.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
public class HomeServiceImpl {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final JavaMailSender javaMailSender;

    private final UserService userService;

    private int generationProgress = 0;

    @Autowired
    public HomeServiceImpl(JavaMailSender javaMailSender, UserService userService) {
        this.javaMailSender = javaMailSender;
        this.userService = userService;
    }

    public void sendMail() {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {

                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("robin.gobo@gmail.com"));
                mimeMessage.setFrom(new InternetAddress("mail@mycompany.com"));
                mimeMessage.setText("test message");
            }
        };

        try {
            this.javaMailSender.send(preparator);
        } catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }

    }

    public LinkedHashMap<String, Object> generateXUsers(int toGenerate, RedirectAttributes redirectAttributes) {
        Long startTime = System.nanoTime();
        int i = 0;
        int batchSize = 500;
        int numberOfUsers = userService.getAllUsers().size();
        User user = null;
        String username;
        String email;
        String password;
        List<User> toAddUsers = new ArrayList<>();
        // generation progress
        generationProgress = 0;
        int totalSteps = (int) (toGenerate / batchSize);
        int currentStep = 0;
        if (totalSteps == 0){
            totalSteps = 1;
        }

        do {
            username = getRandomString(18);
            email = getRandomString(10) + "@" + getRandomString(5) + "." + getRandomString(3);
            password = getRandomString(10);
            user = new User(username, email, password, password);
            toAddUsers.add(user);
//            userService.save(user, null);
            if (i % batchSize == 0) {
                userService.batchSave(toAddUsers);
                toAddUsers = new ArrayList<>();
                currentStep++;
            }
            i++;
            generationProgress = scaleToRange(0, totalSteps, currentStep);
        } while (i < toGenerate);
//        userService.flush();
        userService.batchSave(toAddUsers);
        Long endTime = System.nanoTime();
        Long duration = endTime - startTime;
        duration = TimeUnit.NANOSECONDS.toMillis(duration);

        numberOfUsers = userService.getAllUsers().size() - numberOfUsers;

        LinkedHashMap<String, Object> returnJson = new LinkedHashMap<>();
        returnJson.put("duration", duration);
        returnJson.put("usersCreated", numberOfUsers);
        generationProgress = 0;
        return returnJson;

    }

    private String getRandomString(int length) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    private int scaleToRange(int min, int max, int x){
        int a = 0;
        int b = 100;
        return ( ( ((b-a)*(x-min))/(max-min) ) + a );
    }

    public LinkedHashMap<String, Object> generateCustomUserJSON(){
        List<Object> users = new ArrayList<>();
        for (User u : userService.getAllUsers()){
            LinkedHashMap<String, Object> t = new LinkedHashMap<>();
            t.put("id", u.getId());
            t.put("username", u.getUsername());
            t.put("email", u.getEmail());
            t.put("image", u.getImage().getUrl());

//            List<String> roles = new ArrayList<>();
//            for (Role r : u.getRoles()){
//                roles.add(r.getName().toString());
//            }
            t.put("roles", u.getRoles());
            users.add(t);
        }

        LinkedHashMap<String, Object> data = new LinkedHashMap<>();
        data.put("draw", "1");
        data.put("recordsTotal", Integer.toString(users.size()));
        data.put("recordsFiltered", Integer.toString(users.size()));
        data.put("data", users);

        return data;
    }

    public int getGenerationProgress() {
        return generationProgress;
    }
}
