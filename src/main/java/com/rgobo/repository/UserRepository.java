package com.rgobo.repository;

import com.rgobo.model.User;
import com.rgobo.model.projection.UserP;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(excerptProjection = UserP.class)
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByUsername(String username);
    Page<User> findByUsernameContains(@Param("username") String username, Pageable pageRequest);

    @Procedure(name = "createUserWithImageProcedure")
    void createUserWithImageProcedure(@Param("username") String username,
                                      @Param("email") String email,
                                      @Param("password") String password,
                                      @Param("enabled") Boolean enabled,
                                      @Param("image_id") Long image_id);

    @Procedure(name = "createUserWithoutImageProcedure")
    void createUserWithoutImageProcedure(@Param("username") String username,
                                         @Param("email") String email,
                                         @Param("password") String password,
                                         @Param("enabled") Boolean enabled);

}
