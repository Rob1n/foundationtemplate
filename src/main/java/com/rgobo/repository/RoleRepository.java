package com.rgobo.repository;

import com.rgobo.model.Role;
import com.rgobo.model.RoleEnum;
import com.rgobo.model.projection.RoleP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(excerptProjection = RoleP.class)
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(RoleEnum name);

}
