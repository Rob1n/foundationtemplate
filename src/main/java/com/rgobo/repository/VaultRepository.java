package com.rgobo.repository;

import com.rgobo.model.VaultEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface VaultRepository extends JpaRepository<VaultEntry, Long> {
    List<VaultEntry> findByAddress(String address);
    List<VaultEntry> findByUsername(String username);
    List<VaultEntry> findByUser_Id(Long id);
}
