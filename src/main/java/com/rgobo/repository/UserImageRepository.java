package com.rgobo.repository;

import com.rgobo.model.UserImage;
import org.springframework.data.jpa.repository.JpaRepository;

//@RestResource(exported = false)
public interface UserImageRepository extends JpaRepository<UserImage, Long> {
}
