<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="partials/_taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="partials/_head.jsp" %>
    <title><spring:message code="error.title"/></title>
</head>
<body>

<div class="container col-md-offset-1 col-md-10">
    <h2><spring:message code="error.subtitle"/></h2>

    <c:if test="${empty exception}">
        <p class="alert alert-danger"><spring:message code="error.404"/></p>
    </c:if>

    <c:if test="${not empty exception}">
        <p><c:out value="${exception}"/></p>
        <p class="alert alert-danger"><c:out value="${message}"/></p>
    </c:if>

    <h3><a href="javascript:history.back()"><spring:message code="error.goback"/></a></h3>
    <h3><a href="${pageContext.request.contextPath}/home.html"><spring:message code="error.returnhome"/></a></h3>
</div>

<%@include file="partials/_footer.jsp" %>
</body>
</html>