<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="partials/_taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="partials/_head.jsp" %>
    <title><spring:message code="users.view.title"/></title>
</head>
<body>
<%@ include file="partials/_nav.jsp" %>

<div class="container col-md-10 col-md-offset-1">

    <div class="row">
        <sec:authorize access="hasRole('ROLE_USER')">
            <h1>Hello <sec:authentication property="name"/>!</h1>
        </sec:authorize>

        <c:if test="${not empty message}">
            <span class="alert alert-success"><c:out value="${message}"/></span>
        </c:if>

    </div>

    <div class="row">

        <div class="col-md-5">
            <h3>User image:</h3>
            <div class="thumbnail" style="width: 50%">
                <c:if test="${empty user.image}">
                    <img src="${pageContext.request.contextPath}/images/user/_default.png" alt="default image"
                         class="img-circle img-responsive"/>
                </c:if>
                <c:if test="${not empty user.image}">
                    <img src="${pageContext.request.contextPath}/images/user/${user.image.url}"
                         alt="${user.username} image" class="img-circle img-responsive"/>
                </c:if>
            </div>
        </div>

        <div class="col-md-7">
            <div class="container col-md-10 col-md-offset-1">
                <h2> Hello again, user <c:out value="${user.username}"/></h2>

                <h3>E-mail: <c:out value="${user.email}"/></h3>

                <div>
                    <p>Principal username : <sec:authentication property="name"/></p>
                    <h4>Your roles: </h4>

                    <sec:authentication property="authorities" var="authorities"/>
                    <c:forEach items="${authorities}" var="authority" varStatus="vs">
                        <p>${authority.authority}</p>
                    </c:forEach>
                </div>

                <h3>
                    <a href="/users/${user.username}/update">
                        <spring:message code="users.view.update"/>
                    </a>
                </h3>

                <h3>
                    <a href="/users/${user.username}/delete"
                       onclick="return confirm('Are you sure you want to delete this user?')">
                        <spring:message code="users.view.delete"/>
                    </a>
                </h3>
            </div>
        </div>

    </div>
</div>


<%@include file="partials/_footer.jsp" %>

</body>
</html>