<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="partials/_taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="partials/_head.jsp" %>
    <title><spring:message code="vault.title"/></title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/aes.js"></script>
</head>
<body>
<%@ include file="partials/_nav.jsp" %>

<div class="container col-md-10 col-md-offset-1">

    <h1><spring:message code="vault.new.subtitle"/></h1>

    <div class="row col-md-8">
        <%@include file="partials/_vault.jsp"%>
    </div>

</div>


<%@include file="partials/_footer.jsp" %>
<script src="${pageContext.request.contextPath}/js/scrypt2.js"></script>
<script> scrypt_module_factory(function (scrypt) {
    $("#vaultEntry").submit(function(e){
        e.preventDefault();

        // An example 128-bit key (16 bytes * 8 bits/byte = 128 bits)
        /*var key = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ];
         var pin = prompt("Please enter the encryption PIN", "at least 4 numbers");
         for (var i = 0; i < key.length; i++) {
         key[i] = key[i] + parseInt(pin[i % pin.length]);
         }*/
        var pin = prompt("Please enter the encryption PIN", "at least 4 numbers");
        var key = scrypt.crypto_scrypt(pin, "salt", 8, 8, 1, 16);
        console.log("key: " + key);

        // Convert text to bytes
        var form = this;
        var passInput = $("#formPassword");
        var pass = passInput.val();
        console.log(pass);

        //var text = 'Text may be any length you wish, no padding is required.';
        var textBytes = aesjs.utils.utf8.toBytes(pass);

        // The counter is optional, and if omitted will begin at 1
        var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
        var encryptedBytes = aesCtr.encrypt(textBytes);

        // To print or store the binary data, you may convert it to hex
        var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
        console.log(encryptedHex);

        passInput.val(encryptedHex);

        form.submit();

        /*
         var form = this;
         var passInput = $("#formPassword");

         var pass = passInput.val();
         console.log(pass);
         console.log(this);

         //TODO instead of a random key, ask for user PIN
         var genkey = Generate_key();
         console.log("genkey: " + genkey);

         var ciphertext = Encrypt_Text(pass, genkey);
         console.log("ciphertext: " + ciphertext);

         passInput.val(ciphertext);


         form.submit();
         */
    });
}); </script>

</body>
</html>