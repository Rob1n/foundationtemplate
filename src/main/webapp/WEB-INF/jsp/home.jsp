<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="partials/_taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="partials/_head.jsp" %>
    <title><spring:message code="home.title"/></title>
</head>
<body>
<%@ include file="partials/_nav.jsp" %>

<div class="container col-md-10 col-md-offset-1">
    <div class="row">
        <h1><spring:message code="home.subtitle"/></h1>
        <p>Hello world</p>
    </div>

    <div class="row">
        <sec:authorize access="hasRole('ROLE_USER')">
            <spring:message code="home.welcome"/> <sec:authentication property="name"/>!
        </sec:authorize>
    </div>

    <div class="container col-md-4">

        <div class="row" style="margin-top: 10px;">
            <form:form action="/generate" id="generateForm">
                <h4>Generate users</h4>
                <p>How many?</p>
                <input type="text" name="toGenerate" value="50" id="toGenerate">

                <input type="submit" value="Generate many users" name="submit" id="submitGenerate"/>
            </form:form>
        </div>

        <div class="row">
            <div class="progress" style="margin-top: 20px;">
                <div class="progress-bar" id="progressBarId" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    <span class="sr-only" id="progressBarText">0% Complete</span>
                </div>
            </div>
        </div>

        <div class="row">
            <h4 class="alert alert-warning hidden" id="workingMessage"></h4>
            <h4 class="alert alert-success hidden" id="returnMessage"></h4>
        </div>

        <div class="row">
            <form:form action="/logout" method="POST">
                <spring:message code="nav.logout" var="signOutMessage"/>
                <input type="submit" value="${signOutMessage}" class="btn btn-link"/>
            </form:form>
        </div>

    </div>


</div>

<%@include file="partials/_footer.jsp" %>
<script type="text/javascript" charset="utf8"
        src="${pageContext.request.contextPath}/js/home.js"></script>
</body>
</html>