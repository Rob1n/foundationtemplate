<div class="container col-md-12">
	<div class="row">
		<div class="navbar">
			<div class="navbar-inner">
				<ul class="nav navbar-nav">
					<li>
						<a href="?lang=en">English</a>
					</li>
					<li>
						<a href="?lang=it">Italian</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>