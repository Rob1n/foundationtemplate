<form:form commandName="user" enctype="multipart/form-data">
    <form:errors path="*" cssClass="alert alert-danger" element="div"/>
    <div class="row">
        <div class="col-md-8">
            <h3>Account information:</h3>
            <table class="table">
                <tr>
                    <td><spring:message code="users.new.username"/></td>
                    <td><form:input path="username"/></td>
                    <td><form:errors path="username" cssClass="alert-danger"/></td>
                </tr>
                <tr>
                    <td><spring:message code="users.new.email"/></td>
                    <td><form:input path="email"/></td>
                    <td><form:errors path="email" cssClass="alert-danger"/></td>
                </tr>
                <tr>
                    <td><spring:message code="users.new.password"/></td>
                    <td><form:password path="password"/></td>
                    <td><form:errors path="password" cssClass="alert-danger"/></td>
                </tr>
                <tr>
                    <td><spring:message code="users.new.confirmPassword"/></td>
                    <td><form:password path="confirmPassword"/></td>
                    <td><form:errors path="confirmPassword" cssClass="alert-danger"/></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br/>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <spring:message code="users.new.submitCreate" var="create"/>
                        <input type="submit" value="${create}" class="btn btn-primary"/>
                    </td>
                </tr>

            </table>
        </div>

        <div class="col-md-4">
            <h3>User image:</h3>

            <div class="thumbnail">
                <c:if test="${empty user.image}">
                    <img src="${pageContext.request.contextPath}/images/user/_default.png" alt="default image"
                         class="img-circle img-responsive"/>
                </c:if>
                <c:if test="${not empty user.image}">
                    <img src="${pageContext.request.contextPath}/images/user/${user.image.url}"
                         alt="${user.username} image" class="img-circle img-responsive"/>
                </c:if>
            </div>

            <div>
                <p>File to upload:</p>
                <input type="file" name="file"/>
            </div>

        </div>
    </div>
</form:form>

