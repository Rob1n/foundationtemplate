<nav class="navbar navbar-default">
	<div class="navbar-inner">
		<a class="navbar-brand" href="${pageContext.request.contextPath}/home.html"><spring:message code="nav.home"/></a>
		<ul class="nav navbar-nav">
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<li><a href="${pageContext.request.contextPath}/users.html"><spring:message code="nav.users"/></a></li>
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_ANONYMOUS')">
				<li>
					<a href="${pageContext.request.contextPath}/login"> <spring:message code="nav.login"/> </a>
				</li>
			</sec:authorize>

			<sec:authorize access="hasRole('ROLE_USER')">
				<li>
					<a href="${pageContext.request.contextPath}/vault"><spring:message code="nav.vault"/> </a>
				</li>
			</sec:authorize>

			<sec:authorize access="hasRole('ROLE_USER')">
				<sec:authentication var="username" property="name" />
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<c:out value="${username}" /> <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="${pageContext.request.contextPath}/users/${username}">
								<c:out value="${username}" /> <spring:message code="nav.profile"/>
							</a>
						</li>
						<li>
							<form:form action="/logout" method="POST">
								<spring:message code="nav.logout" var="signOutMessage"/>
								<input type="submit" value="${signOutMessage}" class="btn btn-link" />
							</form:form>
						</li>
					</ul>
				</li>
			</sec:authorize>

		</ul>

	</div>
</nav>