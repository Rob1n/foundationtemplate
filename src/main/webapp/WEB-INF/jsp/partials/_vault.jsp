<form:form commandName="vaultEntry" enctype="multipart/form-data">
    <form:errors path="*" cssClass="alert alert-danger" element="div"/>
    <div class="row">
        <div class="col-md-8">
            <h3>Account information:</h3>
            <table class="table">
                <tr>
                    <td><spring:message code="vault.table.address"/></td>
                    <td><form:input path="address"/></td>
                    <td><form:errors path="address" cssClass="alert-danger"/></td>
                </tr>
                <tr>
                    <td><spring:message code="vault.table.username"/></td>
                    <td><form:input path="username"/></td>
                    <td><form:errors path="username" cssClass="alert-danger"/></td>
                </tr>
                <tr>
                    <td><spring:message code="vault.table.password"/></td>
                    <td><form:password path="password" id="formPassword"/></td>
                    <td><form:errors path="password" cssClass="alert-danger"/></td>
                </tr>
                <tr>
                    <td><spring:message code="vault.table.description"/></td>
                    <td><form:input path="description"/></td>
                    <td><form:errors path="description" cssClass="alert-danger"/></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br/>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <spring:message code="vault.new.submitCreate" var="create"/>
                        <input type="submit" value="${create}" class="btn btn-primary"/>
                    </td>
                </tr>

            </table>
        </div>

    </div>
</form:form>

