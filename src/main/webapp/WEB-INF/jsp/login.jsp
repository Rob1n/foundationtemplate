<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="partials/_taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="partials/_head.jsp" %>
    <title><spring:message code="login.title"/></title>
</head>
<body>
<%@ include file="partials/_nav.jsp" %>

<div class="container col-md-8 col-md-offset-1">

    <div class="row">
        <div>
            <c:if test="${param.error ne null}">
                <h4 class="alert alert-danger"><spring:message code="login.response.invalidlogin"/></h4>
            </c:if>

            <c:if test="${param.logout ne null}">
                <h4 class="alert alert-warning"><spring:message code="login.response.loggedout"/></h4>
            </c:if>

            <c:if test="${not empty message}">
                <h4 class="alert alert-success"><c:out value="${message}"/></h4>
            </c:if>

            <c:if test="${not empty errorMessage}">
                <h4 class="alert alert-danger"><c:out value="${errorMessage}"/></h4>
            </c:if>
            
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <h3><spring:message code="login.subtitle"/></h3>

            <div>
                <form:form action="/login" method="POST" cssClass="form">
                    <table class="table">
                        <tr>
                            <td><label for="username"> <spring:message code="login.form.username"/></label></td>
                            <td><input id="username" type="text" name="username"/></td>
                        </tr>
                        <tr>
                            <td><label for="password"> <spring:message code="login.form.password"/></label></td>
                            <td><input id="password" type="password" name="password"/></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="remember-me"><spring:message code="login.remember"/></label>
                            </td>
                            <td>
                                <input type="checkbox" name="remember-me" id="remember-me"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                <spring:message code="login.submitLogin" var="signInMessage"/>
                                <input type="submit" value="${signInMessage}" class="btn btn-primary"/>
                            </td>
                        </tr>
                    </table>
                </form:form>
            </div>

        </div>

        <div class="col-md-6">
            <h4><a href="${pageContext.request.contextPath}/register"> <spring:message code="login.createNewAccount"/> </a></h4>
            <h4><a href="#"><spring:message code="login.forgotPassword"/> </a></h4>
        </div>

    </div>
</div>

<%@include file="partials/_footer.jsp" %>

</body>
</html>