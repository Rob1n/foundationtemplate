<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="partials/_taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="partials/_head.jsp" %>
    <title><spring:message code="vault.title"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/dataTables/jqueryDataTables.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/aes.js"></script>
</head>
<body>
<%@ include file="partials/_nav.jsp" %>

<div class="container col-md-10 col-md-offset-1">

    <h3><spring:message code="vault.subtitle"/></h3>
    <h4><a href="${pageContext.request.contextPath}/vault/new">Add new entry</a></h4>

    <div class="container col-md-8 col-md-offset-1">

        <table id="vaultTable" class="display table table-striped">
            <thead>
                <tr>
                    <th><spring:message code="vault.table.id"/></th>
                    <th><spring:message code="vault.table.address"/></th>
                    <th><spring:message code="vault.table.username"/></th>
                    <th><spring:message code="vault.table.password"/></th>
                    <th><spring:message code="vault.table.description"/></th>
                    <th><spring:message code="vault.table.delete"/> </th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${vaultEntries}" var="vaultEntry" varStatus="vs">
                    <tr>
                        <td>${vaultEntry.id}</td>
                        <td>${vaultEntry.address}</td>
                        <td>${vaultEntry.username}</td>
                        <td><a href="#" class="passwordField">${vaultEntry.password}</a> </td>
                        <td>${vaultEntry.description}</td>
                        <td><a href="${pageContext.request.contextPath}/vault/${vaultEntry.id}/delete">x</a> </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

    </div>


</div>


<%@include file="partials/_footer.jsp" %>
<script type="text/javascript" charset="utf8"
        src="${pageContext.request.contextPath}/dataTables/jqueryDataTables.js"></script>
<script type="text/javascript" charset="utf8"
        src="${pageContext.request.contextPath}/dataTables/jqueryDataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/js/scrypt2.js"></script>

<script> scrypt_module_factory(function (scrypt) {
    $(".passwordField").click(function(e){
        var encrypted = this.innerHTML;
        console.log("enc: " + encrypted);

        /*
         var key = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ];
         var pin = prompt("Please enter the encryption PIN", "at least 4 numbers");
         for (var i = 0; i < key.length; i++) {
         key[i] = key[i] + parseInt(pin[i % pin.length]);
         }
         */
        var pin = prompt("Please enter the encryption PIN", "at least 4 numbers");
        var key = scrypt.crypto_scrypt(pin, "salt", 8, 8, 1, 16);
        console.log("key: " + key);

        // When ready to decrypt the hex string, convert it back to bytes
        var encryptedBytes = aesjs.utils.hex.toBytes(encrypted);

        // The counter mode of operation maintains internal state, so to
        // decrypt a new instance must be instantiated.
        var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
        var decryptedBytes = aesCtr.decrypt(encryptedBytes);

        // Convert our bytes back into text
        var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
        console.log(decryptedText);

        this.innerHTML = decryptedText;

    });
}); </script>

<script>
    $(document).ready(function(){
        $('#vaultTable').DataTable();
    });


</script>
</body>
</html>