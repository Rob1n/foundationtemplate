<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="partials/_taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="partials/_head.jsp" %>
    <title><spring:message code="users.index.title"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/dataTables/jqueryDataTables.css">

</head>
<body>
<%@ include file="partials/_nav.jsp" %>

<div class="container col-md-8 col-md-offset-1">

    <table id="usersTable" class="display table table-striped table-bordered">
        <thead>
        <tr>
            <th><spring:message code="users.index.userId"/></th>
            <th><spring:message code="users.index.imageId"/></th>
            <th><spring:message code="users.index.name"/></th>
            <th><spring:message code="users.index.email"/></th>
            <th><spring:message code="users.index.roles"/></th>
            <th>Options</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td colspan="6" align="center"><spring:message code="users.index.loading"/></td>
        </tr>
        </tbody>
    </table>

</div>

<%@include file="partials/_footer.jsp" %>
<script type="text/javascript" charset="utf8"
        src="${pageContext.request.contextPath}/dataTables/jqueryDataTables.js"></script>
<script type="text/javascript" charset="utf8"
        src="${pageContext.request.contextPath}/dataTables/jqueryDataTables.bootstrap.js"></script>
<script type="text/javascript" charset="utf8"
        src="${pageContext.request.contextPath}/js/users.js"></script>
</body>
</html>

