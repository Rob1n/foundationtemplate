<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="partials/_taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="partials/_head.jsp" %>
    <title><spring:message code="users.index.title"/></title>
</head>
<body>
<%@ include file="partials/_nav.jsp" %>

<div class="container col-md-10 col-md-offset-1">

    <h1><spring:message code="users.new.subtitle"/></h1>

</div>


<%@include file="partials/_footer.jsp" %>
</body>
</html>