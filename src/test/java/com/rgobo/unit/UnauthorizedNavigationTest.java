package com.rgobo.unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UnauthorizedNavigationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testGetHome_should_get_home_url() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
        mvc.perform(MockMvcRequestBuilders.get("/home").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetUsers_should_get_users_url() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/users").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
//                .andExpect(redirectedUrl("http://localhost/login"))
//                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testGetLogin_should_get_login_url() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/login").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

//    @Test
//    public void testSuccessfulGetUsers() throws Exception {
//        RequestBuilder requestBuilder = formLogin().user("admin").password("admin");
//        mvc.perform(requestBuilder).andExpect(redirectedUrl("/home")).andExpect(status().is5xxServerError());
//    }

}

