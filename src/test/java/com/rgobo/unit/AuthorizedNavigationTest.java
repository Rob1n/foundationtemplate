package com.rgobo.unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration
@WebAppConfiguration
public class AuthorizedNavigationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser(username = "user", roles = {"USER"})
    public void testGetHome_should_get_home_url() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
        mvc.perform(MockMvcRequestBuilders.get("/home").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = {"USER"})
    public void testGetUsers_should_get_users_url() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/users").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = {"USER"})
    public void testGetLogin_should_get_login_url() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/login").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = {"USER"})
    public void testGetLogout_should_get_logged_out_and_redirected_to_login_url() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/logout").accept(MediaType.TEXT_HTML))
//                .andExpect(redirectedUrl("http://localhost/login?logout"));
                .andExpect(status().is3xxRedirection());
    }

}
