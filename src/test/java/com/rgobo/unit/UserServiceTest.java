package com.rgobo.unit;

import com.rgobo.model.User;
import com.rgobo.repository.RoleRepository;
import com.rgobo.repository.UserImageRepository;
import com.rgobo.repository.UserRepository;
import com.rgobo.service.image.ImageService;
import com.rgobo.service.image.ImageServiceImpl;
import com.rgobo.service.role.RoleService;
import com.rgobo.service.role.RoleServiceImpl;
import com.rgobo.service.user.UserService;
import com.rgobo.service.user.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    private UserService userService;
    private UserRepository userRepository;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Before
    public void before() {
        RoleRepository roleRepository = mock(RoleRepository.class);
        RoleService roleService = new RoleServiceImpl(roleRepository);

        UserImageRepository userImageRepository = mock(UserImageRepository.class);
        ImageService imageService = new ImageServiceImpl(userImageRepository);

        this.userRepository = mock(UserRepository.class);
//        this.userService = mock(UserService.class);
        this.userService = new UserServiceImpl(userRepository, passwordEncoder, roleService, imageService);
    }

    @Test
    public void getAllUsers_should_return_all_users() {
        List<User> mockUsers = new ArrayList<>();
        mockUsers.add(new User(1L));
        mockUsers.add(new User(2L));
        mockUsers.add(new User(3L));

        when(userRepository.findAll()).thenReturn(mockUsers);

        List<User> allUsers = userService.getAllUsers();
        assertThat(allUsers.size()).isEqualTo(3);

    }

    @Test
    public void getUser_should_return_one_user() {
        List<User> mockUsers = new ArrayList<>();
        mockUsers.add(new User(1L));
        mockUsers.add(new User(2L));
        mockUsers.add(new User(3L));

        when(userRepository.findOne(2L)).thenReturn(mockUsers.get(1));

        User user = userService.get(2L);
        assertThat(user.getId()).isEqualTo(2L);
    }

    @Test
    public void getUser_should_return_null_if_id_not_found() {
        when(userRepository.findOne(2L)).thenReturn(null);

        User user = userService.get(2L);
        assertThat(user).isEqualTo(null);
    }

    @Test
    public void getUser_should_return_a_proper_user_with_data() {
        User mockUser = new User("username", "email", "password", "password");
        mockUser.setId(1L);

        when(userRepository.findOne(1L)).thenReturn(mockUser);

        User user = userService.get(1L);
        assertThat(user.getId()).isEqualTo(1L);
        assertThat(user.getUsername()).isEqualTo("username");
        assertThat(user.getPassword()).isEqualTo("password");
        assertThat(user.getEmail()).isEqualTo("email");
    }


}
