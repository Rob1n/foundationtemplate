package com.rgobo.bad;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

//@Configuration
//@EnableAutoConfiguration
//@ComponentScan(basePackages = "com.rgobo")
//@EnableJpaRepositories("com.rgobo.model")
//@EnableTransactionManagement
//@Profile("test")
public class TestingConfiguration {

//    @Bean
//    @Profile("test")
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/automaticTesting?useSSL=false");
        dataSource.setUsername("testUser");
        dataSource.setPassword("password");
        return dataSource;
    }

}
