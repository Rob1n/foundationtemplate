package com.rgobo.bad;

import com.rgobo.model.User;
import com.rgobo.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class UserServiceTest extends AbstractTest{

    @Autowired
    private UserService userService;

//    @Test
    public void getOneUser_should_return_one_user(){
        User user = userService.get(1L);
        assertThat(user).isNotNull();
    }

}
