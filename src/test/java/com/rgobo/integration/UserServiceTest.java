package com.rgobo.integration;


import com.rgobo.model.Role;
import com.rgobo.model.RoleEnum;
import com.rgobo.model.User;
import com.rgobo.model.UserImage;
import com.rgobo.service.image.ImageService;
import com.rgobo.service.role.RoleService;
import com.rgobo.service.user.UserService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;

/*
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

 */

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserServiceTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserService userService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private RoleService roleService;

    @Test
    public void test10_CreateUser_should_add_new_user() throws Exception {
        int numberOfUsers = userService.getAllUsers().size();

        User user = new User("testUser", "test@email.com", "password", "password");
        UserImage userImage = imageService.getDefaultImage();
        User newUser = userService.create(user, userImage);
        assertThat(newUser).isNotNull();

        assertThat(user).isEqualToComparingOnlyGivenFields(newUser);
        assertThat(userService.get(3L)).isNotNull();

        assertThat(userService.getAllUsers().size()).isEqualTo(numberOfUsers+1);
    }

    @Test
    public void test11_CreateUser_should_assign_user_role() throws Exception {
        Role userRole = roleService.getRoleByName(RoleEnum.ROLE_USER);
        User user = userService.get(3L);
        assertThat(user.getRoles()).isNotNull();
        assertThat(user.getRoles()).isNotEmpty();
        assertThat(user.getRoles().contains(userRole)).isTrue();
    }

    @Test
    public void test12_CreateUser_should_not_assign_admin_role() throws Exception {
        Role adminRole = roleService.getRoleByName(RoleEnum.ROLE_ADMIN);
        assertThat(userService.get(3L).getRoles().contains(adminRole)).isFalse();
    }

    @Test
    public void test15_CreateUser_should_throw_exception_if_username_already_exists() throws Exception{
        int numberOfUsers = userService.getAllUsers().size();

        User user = new User("admin", "test@email.com", "password", "password");
        // TODO


    }

    @Test
    public void test20_GetUserByName_should_get_user_by_name() throws Exception {
        User storedUser = userService.get(3L);
        User getUser = userService.getUserByName("testUser");
        assertThat(getUser).isNotNull();
        assertThat(getUser).isEqualToComparingOnlyGivenFields(storedUser);
    }

    @Test
    public void test30_GetIdByName_should_get_user_id() throws Exception {
        User storedUser = userService.get(3L);
        assertThat(storedUser).isNotNull();
        Long getId = userService.getIdByName("testUser");
        assertThat(getId).isNotNull();
        assertThat(getId).isEqualTo(storedUser.getId());
    }


    @Test
    public void test40_UpdateUser_should_update_user_fields() throws Exception{
        int numberOfUsers = userService.getAllUsers().size();

        User user = userService.get(3L);
        assertThat(user).isNotNull();

        user.setUsername("testUserUpdate");
        user.setEmail("testUpdate@email.com");
        user.setPassword("passwordUpdate");
        user.setConfirmPassword("passwordUpdate");

        User updatedUser = userService.update(3L, user, user.getImage());
        User newUser = userService.get(3L);

        assertThat(updatedUser).isNotNull();
        assertThat(newUser).isNotNull();

        assertThat(user).isEqualToComparingOnlyGivenFields(updatedUser);
        assertThat(updatedUser).isEqualToComparingOnlyGivenFields(newUser);

        assertThat(userService.getAllUsers().size()).isEqualTo(numberOfUsers);
    }

    @Test
    public void test50_DeleteUser_should_remove_new_user() throws Exception{
        int numberOfUsers = userService.getAllUsers().size();

        User user = userService.get(3L);
        assertThat(user).isNotNull();

        User deletedUser = userService.delete(3L);

        assertThat(deletedUser).isEqualToComparingOnlyGivenFields(user);
        assertThat(userService.get(3L)).isNull();

        assertThat(userService.getAllUsers().size()).isEqualTo(numberOfUsers-1);
    }



}
