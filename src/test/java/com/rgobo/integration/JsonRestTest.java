package com.rgobo.integration;

import com.rgobo.config.TestUtil;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/*
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
*/

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JsonRestTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testGetAllRoles_should_get_all_roles() throws Exception {
        String url = "http://localhost:8080/api/roles";

        mvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(3)))
//                .andExpect(jsonPath("$.*", Matchers.containsInAnyOrder("links", "content", "page")))
                .andExpect(jsonPath("$.links", hasSize(3)))
                .andExpect(jsonPath("$.links[*].rel", Matchers.containsInAnyOrder("self", "profile", "search")))
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[*].id", Matchers.containsInAnyOrder(1, 2)))
                .andExpect(jsonPath("$.content[*].name", Matchers.containsInAnyOrder("ROLE_USER", "ROLE_ADMIN")))
                .andExpect(jsonPath("$.page.totalElements", is(2)))
                .andExpect(jsonPath("$.page.totalPages", is(1)))
        ;

//                .andExpect(jsonPath("$..links[0]", Matchers.containsInAnyOrder("self", "role", "users")))
//                .andExpect(jsonPath("$.content[*].links[0].*", hasSize(3)))
//                .andExpect(jsonPath("$.content[*].links[0]", Matchers.containsInAnyOrder("rel", "self")))
//                .andExpect(jsonPath("$.content[*].links[*].", Matchers.containsInAnyOrder("self", "role", "users")));


//                .andExpect(jsonPath("$['content'].name", is("ROLE_ADMIN")))
//                .andExpect(jsonPath("$['content'].$['link']", hasSize(3)));
//                .andExpect(jsonPath("$['content'].id", is(1)))
//                .andExpect(jsonPath("$[0].id", is(1)))
//                .andExpect(jsonPath("$[0].description", is("Lorem ipsum")))
//                .andExpect(jsonPath("$[0].title", is("Foo")))
//                .andExpect(jsonPath("$[1].id", is(2)))
//                .andExpect(jsonPath("$[1].description", is("Lorem ipsum")))
//                .andExpect(jsonPath("$[1].title", is("Bar")));
//
//        Map<String, Object> map = null;
//        try {
//            map = new Gson().fromJson(readUrl(url), Map.class);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        assertThat(map.containsKey("links")).isTrue();
//        assertThat(map.containsKey("content")).isTrue();
//        assertThat(map.containsKey("page")).isTrue();
//
//        Map<String, Object> content = (Map<String, Object>) map.get("content");
//        assertThat(content.containsKey("")).isTrue();
//
//        content.get
//
//        System.out.println("over");

    }

    @Test
    public void testGetAllUsers_should_get_all_users() throws Exception {
        String url = "http://localhost:8080/api/users?projection=user";

        mvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(3)))

                .andExpect(jsonPath("$.links", hasSize(3)))
                .andExpect(jsonPath("$.links[*].rel", Matchers.containsInAnyOrder("self", "profile", "search")))

                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[*].id", Matchers.containsInAnyOrder(1, 2)))
                .andExpect(jsonPath("$.content[*].username", Matchers.containsInAnyOrder("admin", "foo")))
                .andExpect(jsonPath("$.content[*].email", Matchers.containsInAnyOrder("admin@imenik.hr", "foo@bar.com")))
                .andExpect(jsonPath("$.content[*].image", Matchers.containsInAnyOrder("admin.jpg", "foo.jpg")))

                .andExpect(jsonPath("$.page.totalElements", is(2)))
                .andExpect(jsonPath("$.page.totalPages", is(1)))
        ;

    }

    @Test
    public void testGetAllImages_should_get_all_images() throws Exception{
        String url = "http://localhost:8080/api/userImages";

        mvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(3)))

                .andExpect(jsonPath("$.links", hasSize(2)))
                .andExpect(jsonPath("$.links[*].rel", Matchers.containsInAnyOrder("self", "profile")))

                .andExpect(jsonPath("$.content", hasSize(3)))
                .andExpect(jsonPath("$.content[*].url", Matchers.containsInAnyOrder("_default.png", "admin.jpg", "foo.jpg")))

                .andExpect(jsonPath("$.page.totalElements", is(3)))
                .andExpect(jsonPath("$.page.totalPages", is(1)))
        ;
    }




}
